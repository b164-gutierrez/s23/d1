//Miniactivity 03-22-22


// 3. InsertOne Method
db.hotel.insert(
   {
	name : 'Single',
    accomodate : 2,
    price : 1000,
    description: 'A simple room with all the basic necessities',
    rooms_available: 10,
    isAvailable : false
}
)


// 4. InsertMany
db.hotel.insertMany([
    {
		        name: 'Double',
		        accomodate: 3,
		        price: 2000,
		        description: 'A room fit for a small family going on a vacation',
		        rooms_available: 5,
    			isAvailable : false
		    },
		    {
		       accomodate: 4,
		        price: 4000,
		        description: 'A room with a queen sized bed perfect for a simple getaway',
		        rooms_available: 15,
    			isAvailable : false  
		    }
    
 ])



// 5. Find

db.hotel.find({

	name: 'Double'
})



// 6. UpdateOne

db.hotel.updateOne(
        {description: 'A room with a queen sized bed perfect for a simple getaway'},
        {$set:{rooms_available: 0}
        }
)



// 7. DeleteMany

db.hotel.deleteOne({
	rooms_available: 0
})