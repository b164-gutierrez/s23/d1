/*CRUD Operations
	are the heart of any backend
	This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
*/


//Create (Inserting documents)


//Insert One document

/*Syntax:
	db.collectionName.insertOne({ object })
	db.collectionName.insert({ object })
Javascript Syntax comparison:
	object.object.method({ object })

The mongo shell also uses js for syntax which makes it convenient for us to understand it's code. Creating mongoDB syntax in a text editor makes it easy for us to modify code.
*/

//sample:

db.users.insertMany(
   {
	firstName : Jane,
    lastName : Doe,
    age : 21.0,
    contact : {
        phone : "87654321",
        email : "jane@gmail.com"
    },
    courses : [ 
        "CSS", 
        "Javascript", 
        "Python"
    ],
    department : "none"
}
)


//Insert Many

/*Syntax:
	db.collectionName.insertOne([{ objectA }, {objectB}])
*/

//sample:
db.users.insertMany([
    {
		        firstName: "Stephen",
		        lastName: "Hawking",
		        age: 76,
		        contact: {
		            phone: "87654321",
		            email: "stephenhawking@gmail.com"
		        },
		        courses: [ "Python", "React", "PHP" ],
		        department: "none"
		    },
		    {
		        firstName: "Neil",
		        lastName: "Armstrong",
		        age: 82,
		        contact: {
		            phone: "87654321",
		            email: "neilarmstrong@gmail.com"
		        },
		        courses: [ "React", "Laravel", "Sass" ],
		        department: "none"
		    }
    
 ])